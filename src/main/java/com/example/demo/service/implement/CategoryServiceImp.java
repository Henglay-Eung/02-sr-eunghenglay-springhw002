package com.example.demo.service.implement;

import com.example.demo.model.Article;
import com.example.demo.model.Category;
import com.example.demo.repository.ArticleRepository;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryServiceImp implements CategoryService {
    private CategoryRepository categoryRepository;

    @Autowired
    public void getCategoryRepository(CategoryRepository categoryRepository){
        this.categoryRepository = categoryRepository;
    }
    @Override
    public List<Category> getAll() {
        return categoryRepository.getAllCategories();
    }

    @Override
    public Category getOne(int id) {
        return categoryRepository.getOneCategory(id);
    }


    @Override
    public Category updateCategory(Category category, int id) {
        return categoryRepository.updateCategory(category,id);
    }

    @Override
    public Category deleteCategory(int id) {
        return categoryRepository.deleteCategory(id);
    }

    @Override
    public Category saveCategory(Category category) {
        return categoryRepository.saveCategory(category);
    }
}
