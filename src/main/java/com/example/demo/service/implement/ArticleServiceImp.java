package com.example.demo.service.implement;

import com.example.demo.model.Article;
import com.example.demo.repository.ArticleRepository;
import com.example.demo.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {

    private ArticleRepository articleRepository;
    @Autowired
    public void getArticleRepository(ArticleRepository articleRepository){
        this.articleRepository = articleRepository;
    }
    @Override
    public List<Article> getAll() {
        return articleRepository.getAll();
    }

    @Override
    public Article getOne(int id) {
        return articleRepository.getOne(id);
    }

    @Override
    public List<Article> getArticleByCategoryTitle(String categoryTitle) {
        return articleRepository.getArticleByCategoryTitle(categoryTitle);
    }

    @Override
    public Article updateArticle(Article article, int id) {
        return articleRepository.updateArticle(article,id);
    }

    @Override
    public Article deleteArticle(int id) {
        return articleRepository.deleteArticle(id);
    }

    @Override
    public Article saveArticle(Article article) {
        return articleRepository.saveArticle(article);
    }
}
