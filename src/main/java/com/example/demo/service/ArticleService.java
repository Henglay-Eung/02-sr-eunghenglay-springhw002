package com.example.demo.service;

import com.example.demo.model.Article;

import java.util.List;

public interface ArticleService {
    List<Article> getAll();
    Article getOne(int id);
    List<Article> getArticleByCategoryTitle(String categoryTitle);
    Article updateArticle(Article article, int id);
    Article deleteArticle(int id);
    Article saveArticle(Article article);
}
