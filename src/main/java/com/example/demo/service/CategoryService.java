package com.example.demo.service;

import com.example.demo.model.Article;
import com.example.demo.model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();
    Category getOne(int id);
    Category updateCategory(Category category, int id);
    Category deleteCategory(int id);
    Category saveCategory(Category category);
}
