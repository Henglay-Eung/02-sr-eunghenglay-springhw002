package com.example.demo.controller;

import com.example.demo.model.BaseResponse;
import com.example.demo.model.Category;
import com.example.demo.service.implement.CategoryServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CategoryController {
    private CategoryServiceImp categoryServiceImp;
    @Autowired
    public void getCategoryService(CategoryServiceImp categoryServiceImp){
        this.categoryServiceImp = categoryServiceImp;
    }
    @GetMapping("/categories")
    public ResponseEntity<BaseResponse<List<Category>>> getAllCategories(){
        BaseResponse<List<Category>> response = new  BaseResponse<>();
        List<Category> categoryList = categoryServiceImp.getAll();
        if(categoryList.isEmpty()){
            response.setData(new ArrayList<>());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(categoryList);
            response.setMessage("You have successfully selected the categories");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @GetMapping("/categories/{id}")
    public ResponseEntity<BaseResponse<Category>> getCategoryByID(@PathVariable int id){
        BaseResponse<Category> response = new  BaseResponse<>();
        Category category = categoryServiceImp.getOne(id);
        if(category == null){
            response.setData(new Category());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(category);
            response.setMessage("You have successfully selected the category");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<BaseResponse<Category>> updateCategories(@PathVariable int id,@RequestBody Category categoryReq){
        BaseResponse<Category> response = new  BaseResponse<>();
        Category category = categoryServiceImp.updateCategory(categoryReq, id);
        if(category == null){
            response.setData(new Category());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(category);
            response.setMessage("You have successfully updated the category");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<BaseResponse<Category>> deleteCategory(@PathVariable int id){
        BaseResponse<Category> response = new  BaseResponse<>();
        Category category = categoryServiceImp.deleteCategory(id);
        if(category == null){
            response.setData(new Category());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(category);
            response.setMessage("You have successfully deleted the category");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @PostMapping("/categories")
    public ResponseEntity<BaseResponse<Category>> saveCategory(@RequestBody Category categoryReq){
        BaseResponse<Category> response = new  BaseResponse<>();
        Category category = categoryServiceImp.saveCategory(categoryReq);
        if(category==null){
            response.setData(new Category());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(category);
            response.setMessage("You have successfully selected the category");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
}
