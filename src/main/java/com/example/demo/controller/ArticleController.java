package com.example.demo.controller;

import com.example.demo.model.Article;
import com.example.demo.model.BaseResponse;
import com.example.demo.service.implement.ArticleServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ArticleController {

    private ArticleServiceImp articleServiceImp;
    @Autowired
    public void getArticleService(ArticleServiceImp articleServiceImp){
        this.articleServiceImp = articleServiceImp;
    }
    @GetMapping("/articles")
    public ResponseEntity<BaseResponse<List<Article>>> getAllArticles(){
        BaseResponse<List<Article>> response = new  BaseResponse<>();
        List<Article> articleList = articleServiceImp.getAll();
        if(articleList.isEmpty()){
            response.setData(new ArrayList<>());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(articleList);
            response.setMessage("You have successfully selected the articles");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @GetMapping("/articles/{id}")
    public ResponseEntity<BaseResponse<Article>> getArticleByID(@PathVariable int id){
        BaseResponse<Article> response = new  BaseResponse<>();
        Article article = articleServiceImp.getOne(id);
        if(article == null){
            response.setData(new Article());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(article);
            response.setMessage("You have successfully selected the article");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @GetMapping("/article")
    public ResponseEntity<BaseResponse<List<Article>>> getArticlesByCategoryTitle(@RequestParam("categoryTitle") String categoryTitle){
        BaseResponse<List<Article>> response = new  BaseResponse<>();
        List<Article> article = articleServiceImp.getArticleByCategoryTitle(categoryTitle);
        if(article.isEmpty()){
            response.setData(new ArrayList<>());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(article);
            response.setMessage("You have successfully selected the articles");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @PutMapping("/articles/{id}")
    public ResponseEntity<BaseResponse<Article>> updateArticle(@PathVariable int id,@RequestBody Article articleReq){
        BaseResponse<Article> response = new  BaseResponse<>();
        Article article = articleServiceImp.updateArticle(articleReq, id);
        if(article == null){
            response.setData(new Article());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(article);
            response.setMessage("You have successfully updated the article");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/articles/{id}")
    public ResponseEntity<BaseResponse<Article>> deleteArticle(@PathVariable int id){
        BaseResponse<Article> response = new  BaseResponse<>();
        Article article = articleServiceImp.deleteArticle(id);
        if(article == null){
            response.setData(new Article());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(article);
            response.setMessage("You have successfully deleted the data");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @PostMapping("/articles")
    public ResponseEntity<BaseResponse<Article>> saveArticle(@RequestBody Article articleReq){
        BaseResponse<Article> response = new  BaseResponse<>();
        Article article = articleServiceImp.saveArticle(articleReq);
        if(article==null){
            response.setData(new Article());
            response.setMessage("No records have been found");
            response.setHttpStatus(HttpStatus.NOT_FOUND);

        }else{
            response.setData(article);
            response.setMessage("You have successfully inserted the article");
            response.setHttpStatus(HttpStatus.OK);

        }
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
}
