package com.example.demo.controller;

import com.example.demo.model.Article;
import com.example.demo.model.BaseResponse;
import com.example.demo.model.Category;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/restTemplate")
public class RestTemplateController {

    RestTemplate restTemplate = new RestTemplate();
    @GetMapping("/articles")
    public ResponseEntity<BaseResponse> getAllArticles(){
        ResponseEntity<BaseResponse> response = restTemplate.getForEntity("http://localhost:8080/articles",BaseResponse.class);
        return response;
    }
    @GetMapping("/articles/{id}")
    public ResponseEntity<BaseResponse> getOneArticle(@PathVariable int id){
        ResponseEntity<BaseResponse> response = restTemplate.getForEntity("http://localhost:8080/articles/"+id,BaseResponse.class);
        return response;
    }
    @GetMapping("/article")
    public ResponseEntity<BaseResponse> getArticleByCategoryTitle(@RequestParam("categoryTitle") String categoryTitle){
        ResponseEntity<BaseResponse> response = restTemplate.getForEntity("http://localhost:8080/article?categoryTitle="+categoryTitle,BaseResponse.class);
        return response;
    }
    @PutMapping("/articles/{id}")
    public ResponseEntity<BaseResponse> updateArticle(@PathVariable int id,@RequestBody Article articleReq){
        HttpEntity<Article> articleHttpEntity = new HttpEntity<>(articleReq);
        ResponseEntity<BaseResponse> response = restTemplate.exchange("http://localhost:8080/articles/"+id, HttpMethod.PUT,articleHttpEntity,BaseResponse.class);
        return response;
    }
    @DeleteMapping("/articles/{id}")
    public ResponseEntity<BaseResponse> deleteArticle(@PathVariable int id){
        ResponseEntity<BaseResponse> response = restTemplate.exchange("http://localhost:8080/articles/"+id, HttpMethod.DELETE,null,BaseResponse.class);
        return response;
    }
    @PostMapping("/articles")
    public ResponseEntity<BaseResponse> saveArticle(@RequestBody Article articleReq){
        ResponseEntity<BaseResponse> response = restTemplate.postForEntity("http://localhost:8080/articles",articleReq,BaseResponse.class);
        return response;
    }
    @GetMapping("/categories")
    public ResponseEntity<BaseResponse> getAllCategories(){
        ResponseEntity<BaseResponse> response = restTemplate.getForEntity("http://localhost:8080/categories",BaseResponse.class);
        return response;
    }
    @GetMapping("/categories/{id}")
    public ResponseEntity<BaseResponse> getOneCategory(@PathVariable int id){
        ResponseEntity<BaseResponse> response = restTemplate.getForEntity("http://localhost:8080/categories/"+id,BaseResponse.class);
        return response;
    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<BaseResponse> updateCategory(@PathVariable int id, @RequestBody Category categoryReq){
        HttpEntity<Category> categoryHttpEntity = new HttpEntity<>(categoryReq);
        ResponseEntity<BaseResponse> response = restTemplate.exchange("http://localhost:8080/categories/"+id, HttpMethod.PUT,categoryHttpEntity,BaseResponse.class);
        return response;
    }
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<BaseResponse> deleteCategory(@PathVariable int id){
        ResponseEntity<BaseResponse> response = restTemplate.exchange("http://localhost:8080/categories/"+id, HttpMethod.DELETE,null,BaseResponse.class);
        return response;
    }
    @PostMapping("/categories")
    public ResponseEntity<BaseResponse> saveCategory(@RequestBody Category categoryReq){
        ResponseEntity<BaseResponse> response = restTemplate.postForEntity("http://localhost:8080/categories",categoryReq,BaseResponse.class);
        return response;
    }
}
