package com.example.demo.repository;

import com.example.demo.model.Article;
import com.example.demo.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CategoryRepository {
    @PersistenceContext
    EntityManager entityManager;

    public List<Category> getAllCategories(){

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Category> criteriaQuery = criteriaBuilder.createQuery(Category.class);
        Root<Category> categoryRoot = criteriaQuery.from(Category.class);
        criteriaQuery.select(categoryRoot);
        TypedQuery<Category> categoryTypedQuery = entityManager.createQuery(criteriaQuery);
        return categoryTypedQuery.getResultList();
    }
    public Category getOneCategory(int id){

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Category> criteriaQuery = criteriaBuilder.createQuery(Category.class);
        Root<Category> categoryRoot = criteriaQuery.from(Category.class);
        criteriaQuery.select(categoryRoot);
        criteriaQuery.where(criteriaBuilder.equal(categoryRoot.get("id"),id));
        TypedQuery<Category> categoryTypedQuery = entityManager.createQuery(criteriaQuery);
        Category category = null;
        try{
            category = categoryTypedQuery.getSingleResult();
        }catch (Exception e){
            e.printStackTrace();
        }
        return category;
    }

    @Transactional
    public Category updateCategory(Category category, int id){
        Query query = entityManager.createQuery("update Category c set c.title = :title where c.id = :id");
        query.setParameter("title",category.getTitle());
        query.setParameter("id",id);
        query.executeUpdate();
        entityManager.clear();
        return entityManager.find(Category.class,id);
    }
    @Transactional
    public Category deleteCategory(int id){
        Category category = entityManager.find(Category.class,id);
        Query query = entityManager.createQuery("delete from Category c  where c.id = :id");
        query.setParameter("id",id);
        query.executeUpdate();
        return category;
    }

    @Transactional
    public Category saveCategory(Category category){
        entityManager.persist(category);
        entityManager.clear();
        return entityManager.find(Category.class,category.getId());
    }
}
