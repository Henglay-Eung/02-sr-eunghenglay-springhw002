package com.example.demo.repository;

import com.example.demo.model.Article;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ArticleRepository {
    @PersistenceContext
    EntityManager entityManager;

    public List<Article> getAll(){

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Article> criteriaQuery = criteriaBuilder.createQuery(Article.class);
        Root<Article> articleRoot = criteriaQuery.from(Article.class);
        criteriaQuery.select(articleRoot);
        TypedQuery<Article> articleTypedQuery = entityManager.createQuery(criteriaQuery);
        return articleTypedQuery.getResultList();
    }
    public Article getOne(int id){

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Article> criteriaQuery = criteriaBuilder.createQuery(Article.class);
        Root<Article> articleRoot = criteriaQuery.from(Article.class);
        criteriaQuery.select(articleRoot);
        criteriaQuery.where(criteriaBuilder.equal(articleRoot.get("id"),id));
        TypedQuery<Article> articleTypedQuery = entityManager.createQuery(criteriaQuery);
        Article article = null;
        try{
            article = articleTypedQuery.getSingleResult();
        }catch (Exception e){
            e.printStackTrace();
        }
        return article;
    }


    public List<Article> getArticleByCategoryTitle(String categoryTitle){
        Query query = entityManager.createQuery("select a from Article a where a.category.title = :categoryTitle",Article.class);
        query.setParameter("categoryTitle",categoryTitle);
        return query.getResultList();
    }
    @Transactional
    public Article updateArticle(Article article, int id){
        Query query = entityManager.createQuery("update Article a set a.author = :author,a.title=:title" +
                ",a.description=:description,a.category=:category where a.id = :id");
        query.setParameter("author",article.getAuthor());
        query.setParameter("title",article.getTitle());
        query.setParameter("description",article.getDescription());
        query.setParameter("category",article.getCategory());
        query.setParameter("id",id);
        query.executeUpdate();
        entityManager.clear();
        return entityManager.find(Article.class,id);
    }
    @Transactional
    public Article deleteArticle(int id){
        Article article = entityManager.find(Article.class,id);
        Query query = entityManager.createQuery("delete from Article a  where a.id = :id");
        query.setParameter("id",id);
        query.executeUpdate();
        return article;
    }

    @Transactional
    public Article saveArticle(Article article){
        entityManager.persist(article);
        entityManager.clear();
        return entityManager.find(Article.class,article.getId());
    }
}
